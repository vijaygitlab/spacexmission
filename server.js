/**
 * This Express NodeJS file is kept to test product build and later need to decide about production deployement startegy.
 * 
 * Note: GZip needs to be configured during production deployement if the server is different.
 */
/* eslint-disable */
const express = require('express');
const expressStaticGzip = require("express-static-gzip");
const path = require('path');
const app = express();

const port = process.env.PORT || 3000;
const BUILD_DIR = path.join(__dirname);

app.use(expressStaticGzip(BUILD_DIR));

app.get('*', function (req, res) {
    res.sendFile(path.join(BUILD_DIR, 'index.html'));
});

app.listen(port);
console.log('Server started successfully at ', port, '...');